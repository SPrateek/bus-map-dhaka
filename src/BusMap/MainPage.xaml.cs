﻿namespace BusMap
{
    using System;
    using System.Linq;
    using System.Windows;
    using System.Windows.Navigation;
    using Microsoft.Phone.Controls;
    using Microsoft.Phone.Shell;
    using GalaSoft.MvvmLight.Messaging;
    using Messages_And_Helpers;
    using System.Reflection;
    using System.Device.Location;
    using Microsoft.Phone.Maps.Controls;
    using System.Windows.Shapes;
    using System.Windows.Media;
    using ViewModel;
    using Resources;
    using Microsoft.Phone.Maps.Toolkit;
    using System.Collections.Generic;
    using BusMap.Model;
    

    public partial class MainPage : PhoneApplicationPage
    {
        private ProgressIndicator _ProgressIndicator;
        private GeoCoordinate _MyCoordinate;
        private double _Accuracy;
        private bool _DrawedMapOnce;
        private MapLayer _RadiusLayer;
        private bool _IsNewInstance = true;
        private bool _ZoomEnabledForRadiusView;
        private readonly ViewModelLocator _Locator = new ViewModelLocator();
        private Pushpin LastPushPinTouched = null;
        private MapElement _lastDrawnRoute = null;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            MapExtensionsSetup(MyMap);
            
            Loaded += MainPage_Loaded;

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        #region Overrides
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (!_IsNewInstance) 
                return;

            _IsNewInstance = false;
            _Locator.Main.LoadSettings();
            if (!_Locator.Main.isLocationEnabled) 
                return;

            this.LocationPanel.Visibility = Visibility.Collapsed;
            _Locator.Main.AppBarVisibility = true;
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            this._Locator.Main.SaveSettings();
            this._Locator.Main.BackKeyPressed(e);
            
        }

        #endregion

        #region Loaded Actions
        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Register<EnableDisableControlMessage>(this, (msg) => EnableDisableMessageAction(msg));
            Messenger.Default.Register<InvokeMethodMessage>(this, (msg) => InvokeMethodMessageAction(msg));


            if (_Locator.Main.isLocationEnabled && !_IsNewInstance)
            {
                _Locator.Main.GetCurrentCoordinate();
               
            }
        }

        private void MyMap_Loaded(object sender, RoutedEventArgs e)
        {
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = AppResources.ApplicationID;
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = AppResources.ApplicationToken;

        }

        #endregion

        #region Message

        private object InvokeMethodMessageAction(InvokeMethodMessage msg)
        {
            switch (msg.MethodName)
            {
                case Methods.SetMapView:
                    SetMapView(msg.Parameters[(int)Parameters.Location] as GeoCoordinate, (double)msg.Parameters[(int)Parameters.Accuracy]);
                    break;
                case Methods.ShowProgressIndicator:
                    try { ShowProgressIndicator(msg.Parameters[(int)Parameters.Location] as string); }
                    catch { }
                    break;
                case Methods.HideProgressIndicator:
                    HideProgressIndicator();
                    break;
                case Methods.DrawRoute:
                    DrawRoute(msg.Parameters[0] as Path , (bool)msg.Parameters[1] );
                    break;
                case Methods.RemoveRoute:
                    RemoveRoute();
                    break;
                case Methods.ZoomOntoBusStop:
                    ZoomOntoBusStop(msg.Parameters[(int)Parameters.Location] as BusStop) ;
                    break;
                case Methods.DrawAccuracyRadius:
                    DrawAccuracyRadius(_RadiusLayer, _Locator.Main.MyLocation);
                    break;
            }

            return null;
        }

        

        private object EnableDisableMessageAction(EnableDisableControlMessage msg)
        {
            switch (msg.ControlName)
            {
                case Controls.LocationPanel:
                    this.LocationPanel.Visibility = msg.ControlVisibility;
                    break;
                case Controls.SearchTextBoxTo:
                    if (msg.isFocussed)
                        this.SearchTextBoxFrom.Focus();
                    
                    break;
            }
            return null;
        }
        #endregion


        private void ZoomOntoBusStop(BusStop busStop)
        {
            var MapItemCtrl = GetMapItemsControl();
            if(MapItemCtrl!=null)
            {
                
                MyMap.SetView(busStop.Location, 16, MapAnimationKind.Parabolic);
            }
        }
        private void SetMapView(GeoCoordinate coordinate, double accuracy)
        {
            _Accuracy = accuracy;
            _MyCoordinate = coordinate;
            DrawMapMarkers();
            MyMap.SetView(coordinate, 16, MapAnimationKind.Parabolic);
            _RadiusLayer = DrawAccuracyRadius(_RadiusLayer, _MyCoordinate);
            _ZoomEnabledForRadiusView = true;
        }

        

        private void DrawMapMarkers()
        {
            //MyMap.Layers.Clear();

            _RadiusLayer = new MapLayer();

            //need to make it dynamic to zoom changes doesn't affect it
            // Draw marker for current position
            if (_MyCoordinate != null)
            {

                if (!_DrawedMapOnce)
                    _RadiusLayer = DrawAccuracyRadius(_RadiusLayer, _MyCoordinate);
                else
                {
                    MyMap.Layers.Last().Clear();
                    _RadiusLayer = DrawAccuracyRadius(MyMap.Layers.Last(), _MyCoordinate);
                }
            }
            if (!_DrawedMapOnce)
                MyMap.Layers.Add(_RadiusLayer);


            DrawAllBusStops();
            _DrawedMapOnce = true;
        }

        private MapItemsControl GetMapItemsControl()
        {
            var children = MapExtensions.GetChildren(MyMap);

            var obj = children.FirstOrDefault(x => x is MapItemsControl) as MapItemsControl;

            return obj;
        }

        private void DrawAllBusStops()
        {
            var obj = GetMapItemsControl();

            if (obj != null) 
                obj.ItemsSource = _Locator.Main.BusStopLocations;                
        }

        private void RemoveRoute()
        {
            if (_lastDrawnRoute != null && MyMap.MapElements.Contains(_lastDrawnRoute))
                MyMap.MapElements.Remove(_lastDrawnRoute);
        }

        private void DrawRoute( Path path, bool isAllPathChecked=false)
        {
            RemoveRoute();
                
            if (path != null)
            {
                var polyLine = new MapPolyline { StrokeThickness = 10.0 };
                polyLine.StrokeColor = Colors.Red;
                foreach (var item in path.legs)
                {
                    polyLine.Path.Add(item);
                }

                MyMap.MapElements.Add(polyLine);
                _lastDrawnRoute =polyLine;

                MyMap.Pitch = 20;
                PitchSlider.Value = 20;
                MyMap.SetView(MapExtensionHelper.GetLocationRectangle(path.legs),MapAnimationKind.Parabolic);
                DisableAllTheOtherPoints(path , isAllPathChecked);
                
                
            }  
        
            
        }

        private void DisableAllTheOtherPoints(Path path , bool AllStopagesToggled )
        {
            if (!AllStopagesToggled)
            {
                foreach (var stop in _Locator.Main.BusStopLocations)
                {
                    bool doRemove = true;
                    foreach (var stopage in path.Stopages)
                    {

                        if (stopage == stop.Name)
                        {
                            doRemove = false;
                            stop.isVisible = true;
                        }
                    }
                    if (doRemove)
                        stop.isVisible = false;
                }
            }
            else
            {

            }
        }

        private MapLayer DrawAccuracyRadius(MapLayer mapLayer, GeoCoordinate myCoordinate)
        {
            _Accuracy = _Locator.Main.Accuracy;
            if (_Accuracy < Constants.AccuracyRadiusConstraint)
            {
                var metersPerPixels = (Math.Cos(myCoordinate.Latitude * Math.PI / 180) * 2 * Math.PI * 6378137) / (256 * Math.Pow(2, MyMap.ZoomLevel));
                var radius = _Accuracy / metersPerPixels;

                var ellipse = new Ellipse
                    {
                        Width = radius * 2,
                        Height = radius * 2,
                        Projection = new PlaneProjection() { RotationX = MyMap.Pitch },
                        Fill = new SolidColorBrush(Color.FromArgb(80, 215, 235, 105))
                    };

                if (mapLayer.Count == 0)
                {
                    // The ground resolution (in meters per pixel) varies depending on the level of detail 
                    // and the latitude at which it’s measured. It can be calculated as follows:
                    mapLayer.Add(new MapOverlay
                    {
                        Content = ellipse,
                        GeoCoordinate = new GeoCoordinate(myCoordinate.Latitude, myCoordinate.Longitude),
                        PositionOrigin = new Point(0.5, 0.5)
                    });
                }
                else
                {
                    mapLayer[0].GeoCoordinate = myCoordinate;
                    mapLayer[0].Content = ellipse;
                }
            }

            return mapLayer;
        }

        private void ShowProgressIndicator(string msg)
        {
            if (_ProgressIndicator == null)
            {
                _ProgressIndicator = new ProgressIndicator {IsIndeterminate = true};
            }

            _ProgressIndicator.Text = msg;
            _ProgressIndicator.IsVisible = true;
            SystemTray.SetProgressIndicator(this, _ProgressIndicator);
        }

        private void HideProgressIndicator()
        {
            _ProgressIndicator.IsVisible = false;
            SystemTray.SetProgressIndicator(this, _ProgressIndicator);
        }

        private void PitchValueChanged(object sender, EventArgs e)
        {
            if (PitchSlider == null) 
                return;

            MyMap.Pitch = PitchSlider.Value;
            if(_ZoomEnabledForRadiusView)
                DrawAccuracyRadius(MyMap.Layers.Last(), _MyCoordinate);
        }

        private void HeadingValueChanged(object sender, EventArgs e)
        {
            if (HeadingSlider == null) 
                return;

            var value = HeadingSlider.Value;
            if (value > 360) value -= 360;
            MyMap.Heading = value;
        }

        private void MyMap_ZoomLevelChanged(object sender, MapZoomLevelChangedEventArgs e)
        {
            if (_ZoomEnabledForRadiusView)
                DrawAccuracyRadius(_RadiusLayer, _MyCoordinate);
        }

        private void MapExtensionsSetup(Map map)
        {
            var children = MapExtensions.GetChildren(map);
            var runtimeFields = this.GetType().GetRuntimeFields();

            foreach (var i in children)
            {
                var info = i.GetType().GetProperty("Name");

                if (info != null)
                {
                    var name = (string)info.GetValue(i);

                    if (name != null)
                    {
                        foreach (var j in runtimeFields.Where(j => j.Name == name))
                        {
                            j.SetValue(this, i);
                            break;
                        }
                    }
                }
            }
        }

        private void Pushpin_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            
            if(e.OriginalSource.GetType()== typeof(Ellipse))
            {
                e.Handled = true;
                return;
            }
            bool sendnull = false;

            if (LastPushPinTouched != null && LastPushPinTouched != sender as Pushpin)
            {
                ChangePushPinStyle(LastPushPinTouched, false);
                ChangePushPinStyle(sender as Pushpin, true);
                MyMap.SetView(((Pushpin)sender).GeoCoordinate, 16, MapAnimationKind.Parabolic);
            }

            else if (LastPushPinTouched == sender as Pushpin)
            {
                ChangePushPinStyle(LastPushPinTouched, false);
                LastPushPinTouched = null;
                sendnull = true;
            }
            else
            {
                ChangePushPinStyle(sender as Pushpin, true);
                MyMap.SetView(((Pushpin)sender).GeoCoordinate, 16, MapAnimationKind.Parabolic);
            }

            if(!sendnull)
                LastPushPinTouched = sender as Pushpin;

            
   
           e.Handled = true;
        }

        private void ChangePushPinStyle(Pushpin pushpinref, bool Maximize)
        {
            if (Maximize)
            {
                pushpinref.Style = Application.Current.Resources[StylesAndTemplates.MaximizedPushpin] as Style;
            }
            else
                pushpinref.Style = Application.Current.Resources[StylesAndTemplates.CustomizedPushpin] as Style;
        }

        private void MyMap_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (LastPushPinTouched != null)
                ChangePushPinStyle(LastPushPinTouched, false);
        }



       
    }
}