namespace BusMap.ViewModel
{
    using Messages_And_Helpers;
    using Resources;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using System;
    using System.Device.Location;
    using System.Windows;
    using Windows.Devices.Geolocation;
    using System.IO.IsolatedStorage;
    using System.Collections.ObjectModel;
    using Model;
    using System.Collections.Generic;
    using Microsoft.Phone.Maps.Toolkit;
    using System.Windows.Controls;
    using Coding4Fun.Toolkit.Controls;
    using BusMap.UserControls;
    using Microsoft.Phone.Tasks;
    using System.Linq;
    using System.Windows.Controls.Primitives;
    using System.Windows.Threading;
    using Microsoft.Phone.Maps.Services;
    using System.Threading.Tasks;
    using System.Net;
using System.Collections;

    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        /// 

        #region Togglers
        public bool isLocationEnabled = false;
        private bool noNetworkOrLocation = true;
        private bool isTracking = false;
        private bool isOutOfDhaka = false;
        private bool firstRun = true;
        #endregion


        private bool _AppBarVisibility;
        public bool AppBarVisibility { get { return _AppBarVisibility; } set { _AppBarVisibility = value; RaisePropertyChanged("AppBarVisibility"); } }
        
        //Would be used to trigger the storyboard to make the sliders visible.
        private bool _SliderVisibility = false;
        public bool SliderVisibility { get { return SliderVisibility; } set { _SliderVisibility = value; RaisePropertyChanged("SliderVisibility"); } }

        private double _Accuracy;
        public double Accuracy { get { return _Accuracy; } set { _Accuracy = value; RaisePropertyChanged("Accuracy"); } }

        private GeoCoordinate _MyCoordinate;
        private readonly IsolatedStorageSettings _Settings;
        private ObservableCollection<BusStop> _BusStopLocations;

        private ObservableCollection<BusStop> _FavoriteBusStops=new ObservableCollection<BusStop>();
        public ObservableCollection<BusStop> FavoriteBusStops { get { return _FavoriteBusStops; } set { _FavoriteBusStops = value; RaisePropertyChanged("FavoriteBusStops"); } }
         
        private BusStops _BusStops;
        private RouteDataCollection _RouteDataCollection;

        //select between found routes
        private List<Path> _ReturnedPathCollection = null;
        private int pathCounter = 0;

        //hack for showing the correct bus Routes in showingall busnames for a stop
        private BusStop _LastTappedBusStop = null;

        private Geolocator geoLocator;
        private GeoCoordinateWatcher Watcher;

        public bool IsAllStopagesChecked { get; set; }


        public RelayCommand AllowLocationCommand { get; private set; }
        public RelayCommand GetMyLocationCommand { get; private set; }
        public RelayCommand<object> GetDirectionToPushPinCommand { get; private set; }
        public RelayCommand<object> GetAllBusNamesForABusStopCommand { get; private set; }
        public RelayCommand<string> ShowNextRouteCommand { get; private set; }
        public RelayCommand<string> ShowPreviousRouteCommand { get; private set; }
        public RelayCommand RouteShowPanelCloseCommand { get; private set; }
        public RelayCommand AppBarDirectionCommand { get; private set; }
        public RelayCommand FromToDirectionCommand { get; private set; }
        public RelayCommand DirectionCancelCommand { get; private set; }
        public RelayCommand DownloadMapCommand { get; private set; }
        public RelayCommand<object> AddToFavoritesCommand { get; private set; }
        public RelayCommand ShowFavoritesCommand { get; private set; }
        public RelayCommand<object> DeleteFromFavoritesCommand { get; private set; }
        public RelayCommand<object> FavoriteItemTapCommand { get; private set; }
        public RelayCommand ShowAllBusRoutesCommand { get; private set; }
        public RelayCommand<object> ShowBusRouteFromBusStopCommand { get; private set; }
        public RelayCommand AboutPopupCommand { get; private set; }
        public RelayCommand AboutCloseCommand { get; private set; }
        public RelayCommand ToogleOtherBusStopInRoute { get; private set; }
        public RelayCommand ProhibitLocationCommand { get; private set; }
        public RelayCommand<object> ShowFullBusRouteCommand { get; private set; }


        private MessagePrompt MsgPrompt;
        private Popup AboutPoup;


        private bool _RouteShowPanelVisibility = false;
        public bool RouteShowPanelVisibility 
        {
            get { return _RouteShowPanelVisibility; }
            set { _RouteShowPanelVisibility = value; RaisePropertyChanged("RouteShowPanelVisibility"); }
        }

        private string _SelectedRouteBusName = String.Empty;
        public string SelectedRouteBusName { get { return _SelectedRouteBusName; } set { _SelectedRouteBusName = value; RaisePropertyChanged("SelectedRouteBusName"); } }

        private double _SelectedRouteDistance ;
        public double SelectedRouteDistance { get { return _SelectedRouteDistance; } set { _SelectedRouteDistance = value; RaisePropertyChanged("SelectedRouteDistance"); } }



        private bool _RouteSelectionBarVisibility = false;
        public bool RouteSelectionBarVisibility { get { return _RouteSelectionBarVisibility; } set { _RouteSelectionBarVisibility = value; RaisePropertyChanged("RouteSelectionBarVisibility"); } }

        public ObservableCollection<BusStop> BusStopLocations
        {
            get { return _BusStopLocations; }
            set { _BusStopLocations = value; RaisePropertyChanged(Properties.BusStopLocations); }
        }


        private bool _SearchAutoCompleteVisibility = false;
        public bool SearchAutoCompleteVisibility { get { return _SearchAutoCompleteVisibility; } set { _SearchAutoCompleteVisibility = value; RaisePropertyChanged("SearchAutoCompleteVisibility"); } }

        public BusStop SelectedFrom { get; set; }
        public BusStop SelectedTo { get; set; }
        public bool MyLocationCheckBoxChecked { get; set; }

        public GeoCoordinate MyLocation
        {
            get { return _MyCoordinate; }
            set
            {
                _MyCoordinate = value;
                RaisePropertyChanged(Properties.MyLocation);
            }
        }

 
        public MainViewModel()
        {
            SelectedFrom = new BusStop();
            SelectedTo = new BusStop();

            AllowLocationCommand = new RelayCommand(() => AllowLocation());
            GetMyLocationCommand = new RelayCommand(() => GetMyLocation());
            GetDirectionToPushPinCommand = new RelayCommand<object>((param) => GetDirectionToPushPin(param) );
            GetAllBusNamesForABusStopCommand = new RelayCommand<object>((param) => GetAllBusNameForABusStop(param));
            ShowNextRouteCommand = new RelayCommand<string>((param) => ShowNextRoute(param));
            ShowPreviousRouteCommand = new RelayCommand<string>((param)=>ShowPreviousRoute(param));
            RouteShowPanelCloseCommand = new RelayCommand(() => RouteShowPanelClose());
            AppBarDirectionCommand = new RelayCommand(()=> AppBarDirectionAction());
            FromToDirectionCommand = new RelayCommand(()=>FromToDirectionAction());
            DirectionCancelCommand = new RelayCommand(()=>DirectionCancelAction());
            DownloadMapCommand = new RelayCommand(()=>DonwloadMapAction());
            AddToFavoritesCommand = new RelayCommand<object>((param)=>AddToFavoritesAction(param));
            ShowFavoritesCommand = new RelayCommand(()=>ShowFavoritesAction());
            DeleteFromFavoritesCommand = new RelayCommand<object>((param)=>DeleteFromFavoritesAction(param));
            FavoriteItemTapCommand = new RelayCommand<object>((param) => FavoriteItemTapAction(param));
            ShowAllBusRoutesCommand = new RelayCommand(()=>ShowAllBusRoutesAction());
            ShowBusRouteFromBusStopCommand = new RelayCommand<object>((param) => ShowBusRouteFromBusStopAction(param));
            AboutPopupCommand = new RelayCommand(()=>AboutPopupAction());
            AboutCloseCommand = new RelayCommand(()=>AboutCloseAction());
            ToogleOtherBusStopInRouteCommand = new RelayCommand(()=>ToogleOtherBusStopInRouteAction());
            ProhibitLocationCommand = new RelayCommand(()=>ProhibitLocationAction());
            ShowFullBusRouteCommand = new RelayCommand<object>((param)=>ShowFullBusRouteAction(param));
            
            
            _Settings = IsolatedStorageSettings.ApplicationSettings;
            LoadSettings();
            LoadBusStops();
            LoadBusRoutes();
        }

        private object ShowFullBusRouteAction(object param)
        {
            Path path = param as Path;

            pathCounter = _ReturnedPathCollection.IndexOf(path);

            DrawPathsOnView(_ReturnedPathCollection, pathCounter);
            if (MsgPrompt != null)
                MsgPrompt.Hide();

            return null;
        }

        private object ProhibitLocationAction()
        {
            Application.Current.Terminate();
            return null;
        }


        private object ToogleOtherBusStopInRouteAction()
        {
            if (IsAllStopagesChecked)
            {
                foreach (var item in BusStopLocations)
                {
                    item.isVisible = true;
                }
            }
            else //redundant, have to decide what should I do, should I send it straight ti viewmodel or shouldn't I?
            {
                foreach (var stop in BusStopLocations)
                {
                    bool doRemove = true;
                    foreach (var stopage in _ReturnedPathCollection[pathCounter].Stopages)
                    {

                        if (stopage == stop.Name)
                        {
                            doRemove = false;
                            stop.isVisible = true;
                        }
                    }
                    if (doRemove)
                        stop.isVisible = false;
                }

            }
            
            return null;
        }

        private object AboutCloseAction()
        {
            if (AboutPoup != null && AboutPoup.IsOpen)
            {
                AboutPoup.IsOpen = false;
                AppBarVisibility=true;
            }

            return null;
        }

        private object AboutPopupAction()
        {
            AboutPoup = new Popup();
            AboutPoup.Child = new InfoSplash();
            AboutPoup.IsOpen = true;
            AppBarVisibility = false;
            return null;
        }

        private object ShowBusRouteFromBusStopAction(object param)
        {
            string BusName = param as string;
            var BusRoutes = (from item in _RouteDataCollection.AllRouteData
                             where item.bus_name.Equals(BusName) && item.Stopages.Contains(_LastTappedBusStop.Name)
                             select new Path() {BusName=item.bus_name.Equals("BRTC")?item.bus_name+"-"+item.start_point+" to "+item.end_point:item.bus_name, legs=item.Path, Stopages=item.Stopages, Distance=item.route_length }).ToList();

            if (BusRoutes != null && BusRoutes.Count > 0)
            {
                HideMessagePrompt();
                DrawPathsOnView(BusRoutes);
            }
            
            return null;
        }

        private void HideMessagePrompt()
        {
            if (MsgPrompt != null && MsgPrompt.IsOpen)
                MsgPrompt.Hide();
        }

        private object ShowAllBusRoutesAction()
        {
            List<Path> ReturnedPathCollection = new List<Path>();
           
            ReturnedPathCollection = (from Route in _RouteDataCollection.AllRouteData
                                      select new Path() { BusName = Route.bus_name.Equals("BRTC") ? Route.bus_name + "-" + Route.start_point+" to "+Route.end_point : Route.bus_name + "  " + Route.route_no, legs = Route.Path, Stopages = Route.Stopages, Distance = Route.route_length }).OrderBy(x => x.BusName).ToList<Path>();
            _ReturnedPathCollection = ReturnedPathCollection;
            MsgPrompt = new MessagePrompt();

            MsgPrompt.Title = "All Bus Routes";

            AllBusRoutesInfo AllBusRouteInfoCtrl = new AllBusRoutesInfo();
            AllBusRouteInfoCtrl.RouteNamesList.ItemsSource = ReturnedPathCollection;
            
            //DrawPathsOnView(ReturnedPathCollection);

            MsgPrompt.Body = AllBusRouteInfoCtrl;
            MsgPrompt.IsAppBarVisible = false;
            MsgPrompt.IsCancelVisible = false;
            MsgPrompt.Show();

            return null;
        }


        private void CheckWhetherEnteredDhaka(GeoCoordinate location)
        {
            ReverseGeocodeQuery reverseGeocodeQuery = new ReverseGeocodeQuery();
            reverseGeocodeQuery.GeoCoordinate = location;
            reverseGeocodeQuery.QueryAsync();
            reverseGeocodeQuery.QueryCompleted += (s, e) => {
                if (e.Error == null)
                {
                    if (e.Result != null && e.Result.Count > 0)
                    {
                        MapAddress address = e.Result[0].Information.Address;
//#if DEBUG
//                        MessageBox.Show(address.State);
//#endif
                        if (!String.IsNullOrEmpty (address.State.Trim()) && address.State.ToLower().Contains("dhaka"))
                        {
                            if (isOutOfDhaka)
                            {
                                MessageBox.Show(AppResources.JustEnteredDhakaText, AppResources.ApplicationTitle, MessageBoxButton.OK);
                                MyLocation = location;
                                Messenger.Default.Send(new InvokeMethodMessage { MethodName = Methods.ZoomOntoBusStop, Parameters = new object[] { new BusStop() { Location = MyLocation } } });
                            }
                            else
                            {
                                MyLocation = location;
                            }
                            isOutOfDhaka = false;


                        }
                        else if (String.IsNullOrEmpty(address.State.Trim()))
                        {
                            MyLocation = location;
                        }
                        else
                        {
//#if DEBUG
//                            MessageBox.Show(address.State);
//#endif
                            isOutOfDhaka = true;
                            MessageBox.Show(AppResources.NotInDhakaText, AppResources.ApplicationTitle, MessageBoxButton.OK);
                            MyLocation = DefaultLocations.DefaultLocation;
                            Messenger.Default.Send(new InvokeMethodMessage { MethodName = Methods.ZoomOntoBusStop, Parameters = new object[] { new BusStop() { Location = MyLocation } } });

                        }
                    }
                    else
                    {
                        MyLocation = location;
                    }
                }
                else
                {
                    MyLocation = location;
                }

                Messenger.Default.Send(new InvokeMethodMessage { MethodName = Methods.DrawAccuracyRadius });
            };

        }

        private object FavoriteItemTapAction(object param)
        {
            HideMessagePrompt();
            Messenger.Default.Send(new InvokeMethodMessage { MethodName = Methods.ZoomOntoBusStop, Parameters=new object[]{(param as BusStop)} }); return null;
        }

        private object DeleteFromFavoritesAction(object param)
        {
            BusStop busStop = param as BusStop;
            busStop.isFavorite = false;
            FavoriteBusStops.Remove(busStop);
            return null;
        }

        private object ShowFavoritesAction()
        {
           
                MsgPrompt = new MessagePrompt();
                MsgPrompt.Title = "Favorites";
                FavoritesControl favControl = new FavoritesControl();
                if (FavoriteBusStops.Count == 0)
                {
                    favControl.EmptyListMessage.Visibility = Visibility.Visible;
                }
                else
                {
                    favControl.EmptyListMessage.Visibility = Visibility.Collapsed;
                }
                //favControl.LayoutRoot.DataContext = this;
                MsgPrompt.Body = favControl;
                
                MsgPrompt.IsAppBarVisible = false;
                MsgPrompt.IsCancelVisible = false;
                MsgPrompt.Show();
          

            return null;
        }

        private object AddToFavoritesAction(object param)
        {
            
            BusStop busStop = param as BusStop;

            if (FavoriteBusStops == null)
            {
                FavoriteBusStops = new ObservableCollection<BusStop>();
                busStop.isFavorite = true;
                FavoriteBusStops.Add(busStop);
                return null;
            }

            //I really dont know why FavoriteBusItems.Contains() doesn't work here, feeling like a dumb ass
            //Now it's working, even more weirder!
            //if (FavoriteBusStops.Contains(busStop))
            //    return null;
            
            foreach (var item in FavoriteBusStops)
            {
                if (item.Name.Equals(busStop.Name))
                    return null;
            }


            if (FavoriteBusStops.Count < Constants.maxFavNumber)
            {
                busStop.isFavorite = true;
                FavoriteBusStops.Add(busStop);
            }
            else if (FavoriteBusStops.Count >= Constants.maxFavNumber)
            {
                busStop.isFavorite = true;
                FavoriteBusStops.Remove(FavoriteBusStops.First());
                FavoriteBusStops.Add(busStop);
            }
           
            
            return null;
        }

        private object DonwloadMapAction()
        {
            MapDownloaderTask DownloadMapTask = new MapDownloaderTask();
            DownloadMapTask.Show();

            return null;
        }

        private object DirectionCancelAction()
        {
            SearchAutoCompleteVisibility = false;
            return false;
        }

        private object FromToDirectionAction()
        {
            if (MyLocationCheckBoxChecked)
            {
                if (SelectedTo != null)
                {
                    if (SelectedTo.Name != null && BusStopLocations.Contains(SelectedTo))
                    {
                        List<Path> ReturnedPathCollection = MapExtensionHelper.SearchForRoute(MyLocation, SelectedTo);
                        if (DrawPathsOnView(ReturnedPathCollection))
                            SearchAutoCompleteVisibility = false;

                    }
                }
                else
                    MessageBox.Show("Wrong or null value in To or From Field");
            }
            else
            {

                if (SelectedFrom != null && SelectedTo != null)
                {
                    if (SelectedFrom.Name != null && SelectedTo.Name != null && BusStopLocations.Contains(SelectedFrom) && BusStopLocations.Contains(SelectedTo))
                    {
                        if (SelectedFrom != SelectedTo)
                        {

                            var route = MapExtensionHelper.SearchRouteGenereic(SelectedFrom, SelectedTo);
                            //if (route != null)
                            //{
                            //    route = MapExtensionHelper.concatenatePath(route, SelectedTo);

                            //}
                            
                            if (DrawPathsOnView(route))
                            {
                                SearchAutoCompleteVisibility = false;
                            }

                        }
                        else
                        {
                            MessageBox.Show("Duplicate Value on From and To fields");
                        }

                    }
                }
                else
                    MessageBox.Show("Wrong or null value in To or From Field");
            }
            return null;
        }

        private object AppBarDirectionAction()
        {
            
            SearchAutoCompleteVisibility = true;
            var msg = new EnableDisableControlMessage { ControlName = Controls.SearchTextBoxTo, isFocussed=true };
            Messenger.Default.Send(msg);
            return null;

        }

        private object RouteShowPanelClose()
        {
            Messenger.Default.Send(new InvokeMethodMessage { MethodName = Methods.RemoveRoute});
            RouteShowPanelVisibility = false;
            //cant use LINQ as it's a observableCollection
            foreach (var item in BusStopLocations)
                item.isVisible = true;
            return null;
        }

        private object ShowPreviousRoute(string param)
        {
            if (pathCounter > 0)
            {
                pathCounter--;               
            }
            else
            {
                pathCounter = _ReturnedPathCollection.Count-1;
            }
            var msg = new InvokeMethodMessage() { MethodName = Methods.DrawRoute, Parameters = new object[] { _ReturnedPathCollection[pathCounter] , IsAllStopagesChecked } };
            Messenger.Default.Send(msg);
            SelectedRouteBusName = _ReturnedPathCollection[pathCounter].BusName;
            SelectedRouteBusName = _ReturnedPathCollection[pathCounter].BusName;
            SelectedRouteDistance =  _ReturnedPathCollection[pathCounter].Distance;
            return null;
        }

        private object ShowNextRoute(string param)
        {
            if (pathCounter < _ReturnedPathCollection.Count - 1)
            {
                pathCounter++;

            }
            else
            {
                pathCounter = 0;
            }

            
            var msg = new InvokeMethodMessage() { MethodName = Methods.DrawRoute, Parameters = new object[] { _ReturnedPathCollection[pathCounter] , IsAllStopagesChecked} };
            Messenger.Default.Send(msg);
            SelectedRouteBusName = _ReturnedPathCollection[pathCounter].BusName;
            SelectedRouteDistance = _ReturnedPathCollection[pathCounter].Distance;
            return null;
        }

        private object GetAllBusNameForABusStop(object param)
        {
            BusStop Stop = param as BusStop;
            _LastTappedBusStop = Stop;

            MsgPrompt = new MessagePrompt();
            MsgPrompt.Title = "Bus Stop Details";
            BusNamesInfo infoControl = new BusNamesInfo();           
            infoControl.BusStopTitle.Text = Stop.Name;
            infoControl.BusNamesList.ItemsSource = Stop.BusNames.Distinct();
            MsgPrompt.Body = infoControl;
            MsgPrompt.IsAppBarVisible = false;
            MsgPrompt.IsCancelVisible = false;       
            MsgPrompt.Show();
            
            return null;
        }

        private bool DrawPathsOnView(List<Path> ReturnedPathCollection, int counter=0)
        {
            _ReturnedPathCollection = ReturnedPathCollection;

            if (ReturnedPathCollection != null)
            {
                RouteShowPanelVisibility = true;

                if (ReturnedPathCollection.Count > 1)
                {

                    RouteSelectionBarVisibility = true;
                }
                else
                {

                    RouteSelectionBarVisibility = false;
                }

                pathCounter = counter;
                var msg = new InvokeMethodMessage() { MethodName = Methods.DrawRoute, Parameters = new object[] { ReturnedPathCollection[counter] , IsAllStopagesChecked } };
                Messenger.Default.Send(msg);
                SelectedRouteBusName = ReturnedPathCollection[counter].BusName;
                SelectedRouteDistance = ReturnedPathCollection[counter].Distance;
                
                return true;
            }
            else
            {
                MessageBox.Show("No available path");
                return false;
            }
               
        }

        private object GetDirectionToPushPin(object param)
        {
            Messenger.Default.Send(new InvokeMethodMessage { MethodName = Methods.ShowProgressIndicator, Parameters = new object[] { AppResources.SearchingRouteText } });

            BusStop Destination = param as BusStop;

            List<Path> ReturnedPathCollection=MapExtensionHelper.SearchForRoute(MyLocation, Destination);
            

            DrawPathsOnView(ReturnedPathCollection);

            Messenger.Default.Send(new InvokeMethodMessage { MethodName = Methods.HideProgressIndicator });
            
            return null;
        }


        private object GetMyLocation()
        {
            if (isLocationEnabled)
            {
                GetCurrentCoordinate();

            }
            else
            {
                var result = MessageBox.Show(AppResources.LocationUsageQueryText,
                                                          AppResources.ApplicationTitle,
                                                          MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                {
                    isLocationEnabled = true;
                    GetCurrentCoordinate();

                    SaveSettings();

                }
            }

            return null;
        }

        public void TrackLocation()
        {
            Watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
            
            Watcher.MovementThreshold = Constants.GeoMovementThreshhold;           
            Watcher.PositionChanged += Watcher_PositionChanged;
            Watcher.Start();
            isTracking = true;
        }

        void Watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            //if (firstRun)
            //{

            //    AboutPopupAction();
            //    firstRun = false;
            //}

            GeoCoordinate Coordinate = e.Position.Location;
            
           
                CheckWhetherEnteredDhaka(Coordinate);
            
            
        }

        private object AllowLocation()
        {
           

            AppBarVisibility = true;
            var msg = new EnableDisableControlMessage { ControlName = Controls.LocationPanel, ControlVisibility = Visibility.Collapsed };
            Messenger.Default.Send(msg);

            isLocationEnabled = true;
            noNetworkOrLocation = false;
            AppBarVisibility = true;

            GetCurrentCoordinate();
          

            SaveSettings();

            return null;
        }

        public async void GetCurrentCoordinate()
        {
            Messenger.Default.Send(new InvokeMethodMessage { MethodName = Methods.ShowProgressIndicator, Parameters = new object[] { AppResources.SearchingProgressText } });
            
            geoLocator = new Geolocator { DesiredAccuracy = PositionAccuracy.High };
            geoLocator.MovementThreshold = Constants.GeoMovementThreshhold;

            //if (_Settings.Contains(Settings.LastCoordinate) && _Settings[Settings.LastCoordinate] != null)
            //{
            //    var param = new InvokeMethodMessage { MethodName = Methods.SetMapView, Parameters = new object[] { MyLocation = _Settings[Settings.LastCoordinate] as GeoCoordinate, _Accuracy } };
            //    Messenger.Default.Send(param);
            //}

            try
            {
                
                var currentPosition = await geoLocator.GetGeopositionAsync(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(10));
                _Accuracy = currentPosition.Coordinate.Accuracy;

              
                MyLocation = new GeoCoordinate(currentPosition.Coordinate.Latitude, currentPosition.Coordinate.Longitude);
                

                var param = new InvokeMethodMessage { MethodName = Methods.SetMapView, Parameters = new object[] { MyLocation, _Accuracy } };


                Messenger.Default.Send(param);
                
                TrackLocation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(AppResources.LocationDisabledMessageBoxText, AppResources.ApplicationTitle, MessageBoxButton.OK);
                isLocationEnabled = false;
                noNetworkOrLocation = true;

                if (_Settings.Contains(Settings.LastCoordinate) && _Settings[Settings.LastCoordinate] != null)
                {
                    var param = new InvokeMethodMessage { MethodName = Methods.SetMapView, Parameters = new object[] { MyLocation = _Settings[Settings.LastCoordinate] as GeoCoordinate, _Accuracy } };
                    Messenger.Default.Send(param);
                }
                else
                {
                    MessageBox.Show(AppResources.NoPreviousLocationMessage, AppResources.ApplicationTitle, MessageBoxButton.OK);
                    var param = new InvokeMethodMessage { MethodName = Methods.SetMapView, Parameters = new object[] { MyLocation=DefaultLocations.DefaultLocation, _Accuracy=Constants.DefaultAccuracy } };
                    Messenger.Default.Send(param);
                }

                TrackLocation();
                
            }

            Messenger.Default.Send(new InvokeMethodMessage { MethodName = Methods.HideProgressIndicator });

            
        }

       

   
        public void LoadBusStops()
        {
            _BusStops = BusStops.Instance;
            _BusStops.GetAllBusStops();
            this.BusStopLocations = _BusStops.AllBusStops;
            
        }

        public void LoadBusRoutes()
        {
            _RouteDataCollection = RouteDataCollection.Instance;
            _RouteDataCollection.GetAllRouteData();
        }

        public void SaveSettings()
        {
            if (_Settings.Contains(Settings.IsLocationAllowed))
            {
                if ((bool)_Settings[Settings.IsLocationAllowed] != isLocationEnabled)
                {
                    // Store the new value
                    _Settings[Settings.IsLocationAllowed] = isLocationEnabled;
                }

                _Settings[Parameters.Accuracy.ToString()] = _Accuracy;
            }
            else
            {
                _Settings.Add(Settings.IsLocationAllowed, isLocationEnabled);
            }

            if (_Settings.Contains(Settings.LastCoordinate))
            {
                if (MyLocation != null)
                    _Settings[Settings.LastCoordinate] = MyLocation;
            }
            else
            {
                _Settings.Add(Settings.LastCoordinate, MyLocation);
            }

            if (_Settings.Contains(Parameters.Accuracy.ToString()))
            {
                _Settings[Parameters.Accuracy.ToString()] = _Accuracy;
            }
            else
            {
                _Settings.Add(Parameters.Accuracy.ToString(), _Accuracy);
            }

            if (_Settings.Contains(Settings.IsFavoritesAvailable))
            {
                _Settings[Settings.IsFavoritesAvailable] = FavoriteBusStops;
            }
            else
            {
                _Settings.Add(Settings.IsFavoritesAvailable, FavoriteBusStops);
            }
            if (_Settings.Contains(Settings.IsFirstRun))
            {
                _Settings[Settings.IsFirstRun] = firstRun;
            }
            else
            {
                _Settings.Add(Settings.IsFirstRun, firstRun);
            }

        }

        public void LoadSettings()
        {
            if (_Settings.Contains(Settings.IsLocationAllowed))
            {
                isLocationEnabled = (bool)_Settings[Settings.IsLocationAllowed];
            }

            if (noNetworkOrLocation)
            {
                if (_Settings.Contains(Parameters.Accuracy.ToString()))
                    _Accuracy = (double)_Settings[Parameters.Accuracy.ToString()];
                if (_Settings.Contains(Settings.LastCoordinate))
                    MyLocation = _Settings[Settings.LastCoordinate] as GeoCoordinate;
            }

            if (_Settings.Contains(Settings.IsFavoritesAvailable))
            {
                FavoriteBusStops = (ObservableCollection<BusStop>)_Settings[Settings.IsFavoritesAvailable];
            }
            if (_Settings.Contains(Settings.IsFirstRun))
                firstRun = (bool)_Settings[Settings.IsFirstRun];
        }

        public void SyncFavorites()
        {

        }


        public RelayCommand ToogleOtherBusStopInRouteCommand { get; set; }

        public void BackKeyPressed(System.ComponentModel.CancelEventArgs e)
        {
            if (AboutPoup != null && AboutPoup.IsOpen == true)
            {
                AboutCloseAction();
                e.Cancel = true;
            }
            if (SearchAutoCompleteVisibility == true)
            {
                DirectionCancelAction();
                e.Cancel = true;
            }

            if (RouteShowPanelVisibility == true)
            {
                RouteShowPanelClose();
                
                e.Cancel = true;
            }
        }
    }

   
}